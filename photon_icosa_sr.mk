# Inherit some common lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_tablet_wifionly.mk)

# Inherit device configuration for icosa_sr.
include device/nvidia/foster/lineage.mk
TARGET_INIT_VENDOR_LIB := //device/nintendo/icosa_sr:libinit_icosa_sr
$(call inherit-product, device/nintendo/icosa_sr/full_icosa_sr.mk)

PRODUCT_NAME := photon_icosa_sr
PRODUCT_DEVICE := icosa_sr

# Include Photon stuff
include vendor/photon/device-photon.mk
$(call inherit-product, vendor/photon/BoardConfigVendor.mk)

# Include Maru stuff
$(call inherit-product, vendor/maruos/device-maru.mk)
$(call inherit-product, vendor/maruos/BoardConfigVendor.mk)

PHOTON_VERSION := 0.1
PHOTON_BUILD_VERSION := $(PHOTON_VERSION)-$(shell date -u +%Y%m%d)
MARU_VARIANT := photon
